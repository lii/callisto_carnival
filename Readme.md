
Callisto Carnival
=================

This is a space thruster game featuring stunning graphics, AI players and network functionality. The player controls a space ship flying around the solar system, hunting enemies and docking to space stations.

It is written in Java using the LWJGL binding to OpenGL. No game engine has been used, both graphics and game logic have been developed from scratch.

Accessing and and building the source code
------------------------------------------

1. Clone with the `--recursive` flag to include submodules.
2. Import both projects located in the repository root into Eclipse.
3. Run with `cc.app.Bootstrapper` as the main class.

Game play elements
------------------

* Newtonian physics, ships affected by gravity
* Laser and plasma ball guns
* AI players
* Dockable space stations
* Powerups
* Comets
* Radar
* Sun burn damage when too close
* Nice particle engine flames
* Animated explosions
* Modular and extendible design

Screen shots
------------

![Screenshot 1](https://bitbucket.org/lii/callisto_carnival/raw/master/callisto_carnival_4.png)

Menu screen.

![Screenshot 2](https://bitbucket.org/lii/callisto_carnival/raw/master/callisto_carnival_3.png)

Battle outside the New Constantinople space station (fans of the 1993 DOS game Wing Commander: Privateer might find this familiar). 

![Screenshot 3](https://bitbucket.org/lii/callisto_carnival/raw/master/callisto_carnival_2.png)

Approaching the Mir space station orbiting the planet Earth. The trick to a successful docking is to enter into an orbit at the same altitude as the station.

![Screenshot 4](https://bitbucket.org/lii/callisto_carnival/raw/master/callisto_carnival_1.png)

Argh, too close to the sun, burning! Newbies will have this experience a lot.


Credits
-------

Main developer: Jens Lidestrom

AI players: Tommy Carlsson

Menu screen image: Simon Goransson

Many thanks to: Bjorn Lindfors and Emil Pettersson


